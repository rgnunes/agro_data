Projeto de Webbot para coleta de dados do agronegócio.
Este projeto foi criado com intuito de coletar dados referentes ao Agronegócio e torná-los mais acessíveis e fáceis  de consultar.
O projeto consiste em coletar dados do site Cepea/Esalq, realizar o download dos dados em arquivo no formato csv, tratar os dados coletados com a ajuda de um banco de dados ACCESS e apresentá-los em uma ferramenta em formato Excel.
